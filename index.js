const slides = document.querySelectorAll('.slide');
        let currentSlide = 0;

        document.getElementById('next').addEventListener('click', () => {
            slides[currentSlide].classList.remove('active-slide');
            currentSlide = (currentSlide + 1) % slides.length;
            slides[currentSlide].classList.add('active-slide');
        });

        document.getElementById('prev').addEventListener('click', () => {
            slides[currentSlide].classList.remove('active-slide');
            currentSlide = (currentSlide - 1 + slides.length) % slides.length;
            slides[currentSlide].classList.add('active-slide');
        });

