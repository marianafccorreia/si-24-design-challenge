
# RedLight Summer Internship 2024 - Design Challenge

![banner](/assets/banner.png)

#

In this challenge you should **design** and **implement** a **landing page** for a "Francesinha" restaurant.

Your restaurant is located in the heart of Coimbra and serves the famous "Francesinha" dish. It should be known for its delicious and authentic Francesinhas, as well as its vibrant and energetic atmosphere. You can find inspiration in Francesinha restaurants like "Café Santiago", "Lado B", "Yuko Tavern", and "Cervejaria Brasão".

The objective of this challenge is to create a captivating webpage that showcases the restaurant. Your task is to craft an attractive design and incorporate animations, if desired, to leave a lasting impression on potential customers 🤑. It's worth noting that there are only a few authentic Francesinha restaurants in Coimbra, making this opportunity even more unique and special.

#

### Goals

In this page we want to find:

- A short description about the restaurant;
- Chef's specialities;
- The restaurant crew;
- Contacts and location;
- An image carousel or any other dynamic element (optional - only for masters 😎).

Again, we're looking for something simple, creative an well structured.
You can use images of your choice and "Lorem ipsum" whenever you want. Colors and typography are also choosen by you.
Don't overthink it🤔.

#

### Phases

##### 1.Research 🔎
Research and show us similar solutions that you find inspirational. This step is very important to us, don't skip it.
Document style guides like colors, typography, illustrations and/or others, if applicable.

##### 2.Design 🎨
Design a high-fidelity mockup for your proposal. You can use any tool you want such as Figma, Adobe XD, Sketch, etc.

##### 3.Build 💻
Implement the designed page using HTML and CSS (You can also use SCSS, SASS or other type of styling).
If you're a legend, you can also add some JavaScript for animations or any dynamic content you want to do.

#

### Delivery
When you're done, you should fork this repository and upload your work there to share it with us or you can simply send everything in a .zip folder or a WeTransfer link.
